package WebDriverDemo;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class ChDriverSetup {

    public WebDriver webDriver;

    @Before
    public void init(){
        System.setProperty("webdriver.chrome.driver", "drivers/macos/chromedriver");
         webDriver = new ChromeDriver();
    }

    @Test
    public void testCommandsWebDriver(){


        webDriver.get("https://demoqa.com/text-box");

        //getTitle()
        String siteTitle = webDriver.getTitle();
        System.out.println("Titlul este : "+ siteTitle);
        //getCurrentUrl
        String currentUrl = webDriver.getCurrentUrl();
        System.out.println("URL :" + currentUrl);

        // getPageSource()
       String pageSource = webDriver.getPageSource();
        System.out.println("PAGE SOURCE : " + pageSource);
    }

    @Test
    public void navigationCommands(){
        //navigate().to()
        webDriver.navigate().to("https://www.google.com/");
        webDriver.navigate().to("https://www.facebook.com/");
        //navigate().back();
        webDriver.navigate().back();

        //navigate.forward()
        webDriver.navigate().forward();

        //navigate.refresh()
        webDriver.navigate().refresh();
    }

    @Test
    public void findWebElementId(){
        webDriver.get("https://demoqa.com/text-box");

        By userName = new By.ById("userName");
        By userEmail = new By.ById("userEmail");

        WebElement userNameTextBox = webDriver.findElement(userName);
        WebElement userEmailTextBox = webDriver.findElement(userEmail);

        Assert.assertTrue(userNameTextBox.isDisplayed());
        Assert.assertTrue(userEmailTextBox.isDisplayed());

    }

    @Test
    public void findWebElementByLinkAndPartialLink(){
        webDriver.navigate().to("https://demoqa.com/links");

        By linkTestHome = new By.ByLinkText("Home");
        By partialLinkText = new By.ByPartialLinkText("Ho");

        WebElement fullLink = webDriver.findElement(linkTestHome);
        WebElement partialLink = webDriver.findElement(partialLinkText);

        Assert.assertTrue(fullLink.isDisplayed());
        Assert.assertTrue(partialLink.isDisplayed());

    }

    @Test
    public void findElementByName(){
        webDriver.get("https://www.demoqa.com/automation-practice-form");

        By gender = new By.ByName("gender");

        List<WebElement> genderValue = webDriver.findElements(gender);

        for (WebElement element : genderValue){
            System.out.println("Value = " + element.getAttribute("value"));
            System.out.println("Id = " + element.getAttribute("id"));
        }
    }

    @Test
    public void tema(){
        webDriver.get("https://www.demoqa.com/automation-practice-form");

        By element = new By.ByXPath("//*[@class='custom-control custom-radio custom-control-inline']//*[@value='Female']");
        WebElement subjectsText = webDriver.findElement(element);
        webDriver.findElement(new By.ById("gender-radio-2"));
        //System.out.println(subjectsText.getAttribute("value"));
        Assert.assertTrue(subjectsText.isDisplayed());

    }

    @Test
    public void findElementsByCss() {
        webDriver.get("https://demoqa.com/text-box");
        By emailLabel = new By.ByCssSelector("div.col-md-3.col-sm-12>label[id='userEmail-label']");
        By emailInput = new By.ByCssSelector("div>input[placeholder='name@example.com']");

        WebElement emailLabelElement = webDriver.findElement(emailLabel);
        WebElement emailInputElement = webDriver.findElement(emailInput);

        Assert.assertTrue(emailLabelElement.isDisplayed());
        Assert.assertTrue(emailInputElement.isDisplayed());

        List<WebElement> ex = webDriver.findElements(emailLabel);

    }

    @Test
    public void findElementsByTag() {
        webDriver.navigate().to("https://demoqa.com/text-box");
        // selector for page elements
        By labelsFromPage = new By.ByTagName("label");

        List<WebElement> tags = webDriver.findElements(labelsFromPage);

        for (WebElement element : tags){
            System.out.println("Label name is : " + element.getText());
        }
    }

    @Test
    public void findElementsByXpath() {
        webDriver.navigate().to("https://demoqa.com/text-box");

        By fullName = new By.ByXPath("//input[@placeholder='Full Name']");
        By email = new By.ByXPath("//input[@type='email']");

        WebElement name = webDriver.findElement(fullName);

        WebElement emailTexBox = webDriver.findElement(email);

        name.sendKeys("Tila");
        emailTexBox.sendKeys("valentint@pltk.com");

        System.out.println("Ana are mere!");

    }


    @After
    public void after(){
        //webDriver.close();
        webDriver.quit();
    }


}
