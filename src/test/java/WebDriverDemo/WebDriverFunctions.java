package WebDriverDemo;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.List;

public class WebDriverFunctions {

    WebDriver driver;


    @Before
    public void init(){
        System.setProperty("webdriver.chrome.driver", "drivers/macos/chromedriver");
        driver = new ChromeDriver();
    }

    public void test(){
        By fullNameLabel = new By.ById("userName-label");
        driver.findElement(fullNameLabel);
    }

    @Test
    public void sendKeysCommand(){
        driver.navigate().to("https://demoqa.com/automation-practice-form");

        By firstName = new By.ById("firstName");

        WebElement firstNameTextBox = driver.findElement(firstName);

        firstNameTextBox.sendKeys("Test teste ts ");

    }

    @Test
    public void clearCommand(){

        driver.navigate().to("https://demoqa.com/automation-practice-form");
        By fullName = new By.ById("firstName");
        driver.findElement(fullName).sendKeys("Test ");

        driver.findElement(fullName).clear();

        driver.findElement(fullName).sendKeys("Demo ");
    }

    @Test
    public void clickCommand(){

        driver.navigate().to("https://demoqa.com/automation-practice-form");

        // select male from gender
        By male = new By.ByXPath("//label[@for='gender-radio-1']");

        By usernameLabel = new By.ById("userName-label");

        driver.findElement(usernameLabel).click();

        driver.findElement(male).click();

        // daca pe element nu se poate face clik intoarce eroare
    }

    @Test
    public void getTextCommand() {
        driver.navigate().to("https://demoqa.com/automation-practice-form");

        By userName = new By.ById("userName-label");

        WebElement el = driver.findElement(userName);

        System.out.println("Value is =" + el.getText());
    }

    @Test
    public void isDisplayedCommand(){

        driver.navigate().to("https://demoqa.com/automation-practice-form");

        By fullName = new By.ById("firstName");
        Assert.assertTrue(driver.findElement(fullName).isDisplayed());

        // best practice cand nu stim daca elementul este in pagina
        List<WebElement>  elements = driver.findElements(fullName);
        if(elements.size()>0) {
            Assert.assertTrue(elements.get(0).isDisplayed());
        }
    }

    @Test
    public void getAttributeCommand() {
        driver.navigate().to("https://demoqa.com/automation-practice-form");
        By firstName = new By.ById("firstName");

        WebElement firstNameWl = driver.findElement(firstName);
        System.out.println("Vale is =" + firstNameWl.getAttribute("placeholder"));
    }

    @Test
    public void isEnabledCommand(){

        driver.navigate().to("https://demoqa.com/automation-practice-form");
        By fullName = new By.ById("firstName");

        Assert.assertTrue(driver.findElement(fullName).isEnabled());
    }

    @Test
    public void isSelectedCommand(){

        driver.navigate().to("https://demoqa.com/automation-practice-form");
        // By sports = new By.ByXPath("/");
        By sport = new By.ByXPath("//label[@for='hobbies-checkbox-1']");
        WebElement sportsElement = driver.findElement(sport);
        sportsElement.click();
        Assert.assertTrue(sportsElement.isSelected());
    }

    @Test
    public void submitCommand() {
        driver.navigate().to("https://demoqa.com/automation-practice-form");

        By submit = new By.ById("submit");

        driver.findElement(submit).submit();
    }


    @Test
    public void getTagNameCommand() {
        driver.navigate().to("https://demoqa.com/automation-practice-form");
        By firstName = new By.ById("firstName");

        WebElement firstNameWl = driver.findElement(firstName);
        System.out.println("Tag is =" + firstNameWl.getTagName());
    }

    @Test
    public void getCssValueCommand() {
        driver.navigate().to("https://demoqa.com/automation-practice-form");
        By firstName = new By.ById("firstName");

        WebElement firstNameWl = driver.findElement(firstName);
        System.out.println("Tag is =" + firstNameWl.getCssValue("display"));
    }

    @Test
    public void getLocationCommand() {
        driver.navigate().to("https://demoqa.com/automation-practice-form");

        By firstName = new By.ById("firstName");

        WebElement firstNameWl = driver.findElement(firstName);
        System.out.println("Tag is =" + firstNameWl.getLocation());
    }

    @After
    public void after(){
        driver.close();
    }
}
