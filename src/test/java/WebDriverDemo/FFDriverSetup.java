package WebDriverDemo;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FFDriverSetup {
    @Test
    public void setupFirefox(){

        System.setProperty("webdriver.gecko.driver", "drivers/macos/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.google.com");
        driver.quit();
    }
}
