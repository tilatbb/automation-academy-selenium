package WebDriverDemo;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertTrue;

public class XpathAxisAndFunctions {

    WebDriver webDriver;

    @Before
    public void init(){
        System.setProperty("webdriver.chrome.driver", "drivers/macos/chromedriver");
        webDriver = new ChromeDriver();
    }

    @Test
    public void containsFunction(){
        webDriver.get("https://www.demoqa.com/automation-practice-form");

        By firstName = new By.ByXPath("//input[contains(@placeholder,'First')]");
        By lastName = new By.ByXPath("//input[contains(@placeholder,'Last')]");

        Assert.assertTrue(webDriver.findElement(firstName).isDisplayed());
        Assert.assertTrue(webDriver.findElement(lastName).isDisplayed());
    }

    @Test
    public void startWithFunction(){
        //tag_name[starts-with(@attribute,'Part_of_Attribute_value')]
        String x = "ccc";

        webDriver.get("https://www.demoqa.com/automation-practice-form");
        By firstName = new By.ByXPath("//input[starts-with(@placeholder,'" + x +"')]");
        By lastName = new By.ByXPath("//input[starts-with(@placeholder,'La')]");


        Assert.assertTrue(webDriver.findElement(firstName).isDisplayed());
        Assert.assertTrue(webDriver.findElement(lastName).isDisplayed());
    }


    @Test
    public void textFunction(){
        webDriver.get("https://www.demoqa.com/automation-practice-form");

        By nameLabel = new By.ByXPath("//label[text()='Name']");
        By emailLabel = new By.ByXPath("//label[text()='Email']");

        Assert.assertTrue(webDriver.findElement(nameLabel).isDisplayed());
        Assert.assertTrue(webDriver.findElement(emailLabel).isDisplayed());
    }

    @Test
    public void andOrFunctions(){
        webDriver.get("https://www.demoqa.com/automation-practice-form");
        //and
       // By firstName1 = new By.ByXPath("//input[@type='text' and @id='firstName']");
        By firstName1 = new By.ByXPath("//input[@placeholder='First Name' and @id='firstName']");
        //or
        By firstName2 = new By.ByXPath("//input[@placeholder='First Name' or @id='firstName']");

        Assert.assertTrue(webDriver.findElement(firstName1).isDisplayed());
        Assert.assertTrue(webDriver.findElement(firstName2).isDisplayed());

    }

    @Test
    public void ancestorAxis(){
        //tag[@attribute ='Attribute_Value']//ancestor::parent_node
        By userForm = new By.ByXPath("//label[text()='Name']/ancestor::form");

        webDriver.get("https://www.demoqa.com/automation-practice-form");

        System.out.println(webDriver.findElement(userForm).getText());
        System.out.println(webDriver.findElement(userForm).getAttribute("id"));
    }

    @Test
    public void descendantAxis(){
        //node[attribute='value of attribute']//descendant::attribute
        By emailAddress = new By.ByXPath("//div[@id='userEmail-wrapper']/descendant::input");

        webDriver.get("https://www.demoqa.com/automation-practice-form");

        webDriver.findElement(emailAddress).sendKeys("vali@pltk.com");
    }

    @Test
    public void parentAxis(){
        //tag[@attribute ='Attribute_Value']//ancestor::parent_node
        By divEmailAddress = new By.ByXPath("//input[@id='userEmail']//parent::div");

        webDriver.get("https://www.demoqa.com/automation-practice-form");

        assertTrue(webDriver.findElement(divEmailAddress).isDisplayed());
    }

    @Test
    public void childAxis(){
        //tag[@attribute ='Attribute_Value']//child::child_node
        By emailAddress = new By.ByXPath("//form[@id='userForm']/child::div[2]//input");
        webDriver.get("https://www.demoqa.com/automation-practice-form");

        webDriver.findElement(emailAddress).sendKeys("vali@pltk.com");
    }

    @Test
    public void followingAxis(){
        //node[attribute='value of attribute']//following::attribute

        By currentAddress = new By.ByXPath("//input[@id='firstName']/following::textarea");

        webDriver.get("https://www.demoqa.com/automation-practice-form");

        webDriver.findElement(currentAddress).sendKeys("Bulevardul Eroilor");

    }


    @After
    public void after(){
        webDriver.quit();
    }

}
